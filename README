Result from following the step-by-step guide http://kubernetes.io/docs/hellonode/

Requirements
============
        server: centos7 
            Linux sandbox 3.10.0-327.28.3.el7.x86_64 #1 SMP Thu Aug 18 19:05:49 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux
            CentOS Linux release 7.2.1511 (Core)
        Docker 
            version 1.12.1, build 23cf638
        Python 
            Python 2.7.5
        Google Cloud SDK
            Google Cloud SDK 124.0.0
            bq 2.0.24
            bq-nix 2.0.24
            core 2016.08.29
            core-nix 2016.08.29
            gcloud 
            gsutil 4.21
            gsutil-nix 4.21
            kubectl 
            kubectl-linux-x86_64 1.3.


==============
[1] Setting up
==============

        Install Docker
        ==============
                $ sudo yum update
                $ sudo tee /etc/yum.repos.d/docker.repo <<-'EOF'
                [dockerrepo]
                name=Docker Repository
                baseurl=https://yum.dockerproject.org/repo/main/centos/7/
                enabled=1
                gpgcheck=1
                gpgkey=https://yum.dockerproject.org/gpg
                EOF

                $ sudo yum install docker-engine
                $ sudo /bin/systemctl start docker.service
                $ sudo /bin/systemctl status docker.service
                $ sudo docker run hello-world


        Install Google Cloud SDK
        ========================
                First create google project on https://console.cloud.google.com/home/

                Verify Python version is at least 2.7
                    $ python -V
                
                $ wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-124.0.0-linux-x86_64.tar.gz
                $ tar xvf google-cloud-sdk-124.0.0-linux-x86_64.tar.gz 
                $ cd google-cloud-sdk
                $ sudo ./install.sh

        Verify
        ======
                $ gcloud init
                $ gcloud --help
                $ gcloud auth list
                $ gcloud config list

        Install Kubernetes
        =================
                $ gcloud components install kubectl

                    Your current Cloud SDK version is: 124.0.0
                    Installing components from version: 124.0.0

                    ┌──────────────────────────────────────────────┐
                    │     These components will be installed.      │
                    ├─────────────────────────┬─────────┬──────────┤
                    │           Name          │ Version │   Size   │
                    ├─────────────────────────┼─────────┼──────────┤
                    │ kubectl                 │         │          │
                    │ kubectl (Linux, x86_64) │   1.3.5 │ 11.4 MiB │
                    └─────────────────────────┴─────────┴──────────┘


        =======================
        [2] build nodejs server
        =======================

        Build hellonode nodejs
        ======================
                $ mkdir hellonode

                $ tee server.js <<-'EOF' 
                    var http = require('http');
                    var handleRequest = function(request, response) {
                    console.log('Received request for URL: ' + request.url);
                    response.writeHead(200);
                    response.end('Hello World!');
                    };
                    var www = http.createServer(handleRequest);
                    www.listen(8080);
                    EOF

                $ node server.js 

                Open another terminal and test 
                    $ curl http://localhost:8080/

                Expect the server to return "Hello World!" and in the other terminal to print "Received request for URL: /"

        Create Docker container image
        =============================
                $ tee Dockerfile <<-'EOF'
                    FROM node:4.4
                    EXPOSE 8080
                    COPY server.js .
                    CMD node server.js
                    EOF


        Export your project ID
        ======================
                $ export PROJECT_ID="eedevs-kubernetes-1"

        Build the image
        ===============
                $ docker build -t gcr.io/$PROJECT_ID/hello-node:v1 .

                    >>
                    Sending build context to Docker daemon 3.072 kB
                    Step 1 : FROM node:4.4
                    4.4: Pulling from library/node
                    357ea8c3d80b: Pull complete 
                    52befadefd24: Pull complete 
                    3c0732d5313c: Extracting [=======>                                           ]  6.39 MB/42.5 MB
                    ceb711c7e301: Download complete 
                    868b1d0e2aad: Download complete 
                    3a438db159a5: Download complete 
                    Digest: sha256:e720e944ce6994a461cd2a9e0ae34c4bc45c0f9ee7b3f48052933182fc5f0bf1
                    Status: Downloaded newer image for node:4.4
                    ---> 93b396996a16
                    Step 2 : EXPOSE 8080
                    ---> Running in 508d7589f6bb
                    ---> 258374095644
                    Removing intermediate container 508d7589f6bb
                    Step 3 : COPY server.js .
                    ---> 7358d5c3ab18
                    Removing intermediate container 92f5e09f267b
                    Step 4 : CMD node server.js
                    ---> Running in 36de2aa35955
                    ---> 406be2ef210c
                    Removing intermediate container 36de2aa35955
                    Successfully built 406be2ef210c
                    <<<


        Run and test the image
        =====================
                $ docker run -d -p 8080:8080 --name hello_tutorial gcr.io/$PROJECT_ID/hello-node:v1
                    >>>
                    0e9b0d29b4275579e844ed25674856312cfb57a5799ff60d2e1d73d6eb84a33b
                    <<<

        Check the process
        =================
                $ ps -ef | grep docker
                    root      2166     1  2 09:46 ?        00:00:47 /usr/bin/dockerd
                    root      2169  2166  0 09:46 ?        00:00:00 docker-containerd -l unix:///var/run/docker/libcontainerd/docker-containerd.sock --shim docker-containerd-shim --metrics-interval=0 --start-timeout 2m --state-dir /var/run/docker/libcontainerd/containerd --runtime docker-runc
                    root      6974  2166  0 10:12 ?        00:00:00 /usr/bin/docker-proxy -proto tcp -host-ip 0.0.0.0 -host-port 8080 -container-ip 172.17.0.2 -container-port 8080
                    root      6978  2169  0 10:12 ?        00:00:00 docker-containerd-shim 0e9b0d29b4275579e844ed25674856312cfb57a5799ff60d2e1d73d6eb84a33b /var/run/docker/libcontainerd/0e9b0d29b4275579e844ed25674856312cfb57a5799ff60d2e1d73d6eb84a33b docker-runc

        Check using docker
                $ docker ps
                    >>>
                    CONTAINER ID        IMAGE                                      COMMAND                  CREATED             STATUS              PORTS                    NAMES
                    0e9b0d29b427        gcr.io/eedevs-kubernetes-1/hello-node:v1   "/bin/sh -c 'node ser"   2 minutes ago       Up 2 minutes        0.0.0.0:8080->8080/tcp   hello_tutorial
                    <<<

        Verify again that the server is listening and responding
        ========================================================
                $ curl http://localhost:8080
                    >>>
                    Hello World!
                    <<<


        Let's finish up by stopping the service
        =======================================
                $ docker stop hello_tutorial


        Push to repository
        =================
                $ gcloud docker push gcr.io/$PROJECT_ID/hello-node:v1
                    >>>
                    The push refers to a repository [gcr.io/eedevs-kubernetes-1/hello-node]
                    01a65296f769: Pushing [==================================================>] 2.048 kB
                    20a6f9d228c0: Pushing [===============>                                   ] 11.81 MB/37.3 MB
                    80c332ac5101: Pushing [==================================================>] 86.02 kB
                    04dc8c446a38: Pushing [>                                                  ] 1.635 MB/318.1 MB
                    1050aff7cfff: Pushing [===>                                               ] 9.349 MB/122.6 MB
                    66d8e5ee400c: Waiting 
                    2f71b45e4e25: Waiting 
                    <<<>>>
                    The push refers to a repository [gcr.io/eedevs-kubernetes-1/hello-node]
                    01a65296f769: Pushed 
                    20a6f9d228c0: Pushed 
                    80c332ac5101: Pushed 
                    04dc8c446a38: Pushed 
                    1050aff7cfff: Pushed 
                    66d8e5ee400c: Pushed 
                    2f71b45e4e25: Pushed 
                    v1: digest: sha256:0d798ac3bf8667e82aad2db34afce77a5d74a201f5e4d02b910f8bdfa383c231 size: 9402
                    <<<

===============================
[3] Create Google cloud cluster
===============================


        Create Google cluser zone
        =========================

        Set zone from available zone :: https://cloud.google.com/compute/docs/regions-zones/regions-zones 
        =================================================================================================
                    $ gcloud config set compute/zone europe-west1-b

                    $ gcloud container clusters create hello-world
                        >>>
                        Creating cluster hello-world...-
                        Created [https://container.googleapis.com/v1/projects/eedevs-kubernetes-1/zones/europe-west1-b/clusters/hello-world].
                        kubeconfig entry generated for hello-world.
                        NAME         ZONE            MASTER_VERSION  MASTER_IP     MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
                        hello-world  europe-west1-b  1.3.6           104.199.3.88  n1-standard-1  1.3.6         3          RUNNING
                        <<<

                    View cluser on google management console :: https://console.cloud.google.com/compute/instances?project=eedevs-kubernetes-1&graph=GCE_CPU&duration=PT1H
                    =======================================================================================================================================================
                        >>>
                        Name	Zone	Machine type	Recommendation	In use by	Internal IP	External IP	Connect
                        
                        gke-hello-world-default-pool-91caa681-8c2g	europe-west1-b	
                        1 vCPU, 3.75 GB
                        gke-hello-world-default-pool-91caa681-grp
                        10.132.0.2
                        104.155.35.178
                        SSH  
                        
                        gke-hello-world-default-pool-91caa681-9154	europe-west1-b	
                        1 vCPU, 3.75 GB
                        gke-hello-world-default-pool-91caa681-grp
                        10.132.0.4
                        104.199.10.118
                        SSH  
                        
                        gke-hello-world-default-pool-91caa681-9p7k	europe-west1-b	
                        1 vCPU, 3.75 GB
                        gke-hello-world-default-pool-91caa681-grp
                        10.132.0.3
                        104.155.67.104
                        SSH  
                        <<<


        Retrieve credentials
        ====================
                    $ gcloud container clusters get-credentials hello-world
                        >>>
                        Fetching cluster endpoint and auth data.
                        kubeconfig entry generated for hello-world.
                        <<<


=========================
[4] Create Kubernetes pod
=========================

        Display Kubernetes Version
        ==========================
                    $ kubectl version
                        >>>
                        Client Version: version.Info{Major:"1", Minor:"3", GitVersion:"v1.3.5", GitCommit:"b0deb2eb8f4037421077f77cb163dbb4c0a2a9f5", GitTreeState:"clean", BuildDate:"2016-08-11T20:29:08Z", GoVersion:"go1.6.2", Compiler:"gc", Platform:"linux/amd64"}
                        Server Version: version.Info{Major:"1", Minor:"3", GitVersion:"v1.3.6", GitCommit:"ae4550cc9c89a593bcda6678df201db1b208133b", GitTreeState:"clean", BuildDate:"2016-08-26T18:06:06Z", GoVersion:"go1.6.2", Compiler:"gc", Platform:"linux/amd64"}
                        <<<

        Time to create the pod - this is a group of containers, tied together for the purposes of administration and networking. It can contain a single container or multiple.
        [Make sure your PROJECT_ID is set otherwise pods will fail to fetch container "ImagePullBackOff"]
        =================================================================================================
                    $ kubectl run hello-node --image=gcr.io/$PROJECT_ID/hello-node:v1 --port=8080
                        >>>
                        deployment "hello-node" created
                        <<<

        Create deployment plan - this is the recommended way for managing creation and scaling of pods. A Deployment provides declarative updates for Pods and Replica Sets (the next-generation Replication Controller). You only need to describe the desired state in a Deployment object, and the Deployment controller will change the actual state to the desired state at a controlled rate for you. 
        ==========================================================================================
                    $ kubectl get deployments
                        >>>
                        NAME         DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
                        hello-node   1         1         1            0           28s
                        [eedevs@sandbox hellonode]$ kubectl get pods
                        NAME                          READY     STATUS              RESTARTS   AGE
                        hello-node-1926546658-1i90t   0/1       ContainerCreating   0          34
                        <<<

        Display pod status
        ==================
                    $ kubectl get pods
                        >>>
                        NAME                          READY     STATUS    RESTARTS   AGE
                        hello-node-1926546658-1i90t   1/1       Running   0          1m
                        <<<

        Additional tools to use to display information about your cluster/pods
        ======================================================================
                    $ kubectl cluster-info
                        >>>
                        Kubernetes master is running at https://130.211.51.250
                        GLBCDefaultBackend is running at https://130.211.51.250/api/v1/proxy/namespaces/kube-system/services/default-http-backend
                        Heapster is running at https://130.211.51.250/api/v1/proxy/namespaces/kube-system/services/heapster
                        KubeDNS is running at https://130.211.51.250/api/v1/proxy/namespaces/kube-system/services/kube-dns
                        kubernetes-dashboard is running at https://130.211.51.250/api/v1/proxy/namespaces/kube-system/services/kubernetes-dashboard

                        To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
                        <<<


        It is time to expose our cluster to the world by requesting an external IP
        ==========================================================================
                    $ kubectl expose deployment hello-node --type="LoadBalancer"
                        >>>
                        service "hello-node" exposed
                        <<<

                    $ kubectl get services hello-node
                        >>>
                        NAME         CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
                        hello-node   10.19.255.216   <pending>     8080/TCP   13s
                        <<<
                    $ kubectl get services hello-node
                        >>>
                        NAME         CLUSTER-IP      EXTERNAL-IP      PORT(S)    AGE
                        hello-node   10.19.255.216   104.155.79.197   8080/TCP   1m
                        <<<


        Open up a browser or a new shell and test the server
        ====================================================
                    http://104.155.79.197:8080/

        You can check the logs for request
        ==================================
                    $ kubectl logs  hello-node-1926546658-1i90t
                        >>>
                        Received request for URL: /
                        Received request for URL: /favicon.ico
                        Received request for URL: /
                        Received request for URL: /
                        <<<



=======================
[5] Increasing capacity
=======================


        To add more resource/capacity increase the value of --replicas to 2 (two pods)
        ==============================================================================
                    $ kubectl scale deployment hello-node --replicas=2
                        >>>
                        deployment "hello-node" scaled
                        <<<
                    $ kubectl get pods
                        >>>
                        NAME                          READY     STATUS              RESTARTS   AGE
                        hello-node-1926546658-1i90t   1/1       Running             0          7m
                        hello-node-1926546658-exrkf   0/1       ContainerCreating   0          2s
                        <<<


        Verify that you can see two pods running
        ========================================
                    $ kubectl get pods
                        >>>
                        NAME                          READY     STATUS    RESTARTS   AGE
                        hello-node-1926546658-1i90t   1/1       Running   0          8m
                        hello-node-1926546658-exrkf   1/1       Running   0          55s
                        <<<



==============================
[6] Upgrading deployment image
==============================


        Upgrading your docker image and changing deployment image
        =========================================================

        Create new docker image after updating node and push to repo
        ============================================================

                    $ docker build -t gcr.io/$PROJECT_ID/hello-node:v2 .
                        >>>
                        Sending build context to Docker daemon  34.3 kB
                        Step 1 : FROM node:4.4
                        ---> 93b396996a16
                        Step 2 : EXPOSE 8080
                        ---> Using cache
                        ---> 258374095644
                        Step 3 : COPY server.js .
                        ---> 684c0fbd54eb
                        Removing intermediate container c647f35e00c4
                        Step 4 : CMD node server.js
                        ---> Running in e1c36aff7a13
                        ---> c0bfa94a7267
                        Removing intermediate container e1c36aff7a13
                        Successfully built c0bfa94a7267
                        <<<
                    $ gcloud docker push gcr.io/$PROJECT_ID/hello-node:v2
                        >>>
                        The push refers to a repository [gcr.io/eedevs-kubernetes-1/hello-node]
                        2e65b39a8d92: Pushed 
                        20a6f9d228c0: Layer already exists 
                        80c332ac5101: Layer already exists 
                        04dc8c446a38: Layer already exists 
                        1050aff7cfff: Layer already exists 
                        66d8e5ee400c: Layer already exists 
                        2f71b45e4e25: Layer already exists 
                        v2: digest: sha256:c42b83d239ef7b2733094cf4a782b83542484b444147939cc7d2cb7fa05b1074 size: 9402
                        <<<



        Update deployment image
        =======================
                    $ kubectl set image deployment/hello-node hello-node=gcr.io/$PROJECT_ID/hello-node:v2
                        >>>
                        deployment "hello-node" image updated
                        <<<

        There should not be any interruption to user services
        =====================================================
                    $ kubectl get deployments
                        >>>
                        NAME         DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
                        hello-node   2         2         2            1           13m
                        <<<


============
[7] Cleanup
============

        Time to destroy and cleanup
        ===========================

        Delete deployment
        =================
                    $ kubectl delete service,deployment hello-node

        Delete google cluser
        ====================
                    $ gcloud container clusters delete hello-world


        List of images
        ==============
                    $ gsutil ls
                        >>>
                        gs://artifacts.eedevs-kubernetes-1.appspot.com/
                        gs://eedevs-kubernetes-1.appspot.com/
                        <<<

        Remove image from all paths
        ===========================
                    $ gsutil rm -r gs://artifacts.$PROJECT_ID.appspot.com/
                        >>>
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:357ea8c3d80bc25792e010facfc98aee5972ebc47e290eb0d5aea3671a901cab#1472811563634000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:3a438db159a5e17cb9d0f11a192ff6790fd236f252a64aa7e144c827441f6741#1472811503727000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:3c0732d5313c8ec8477e518f3e0af81796bdb047ed48cf256333785fc9916ba1#1472811554934000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:406be2ef210cb76af0204ea578c7dc679b17aa3dc410235c76a9e7f6782cba69#1472811574429000...
                        / [4 objects]                                                                   
                        ==> NOTE: You are performing a sequence of gsutil operations that may
                        run significantly faster if you instead use gsutil -m -o ... Please
                        see the -m section under "gsutil help options" for further information
                        about when gsutil -m can be advantageous.

                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:52befadefd24601247558f63fcb2ccd96b79cbc447a148ea1d0aa2719a9ac3b1#1472811484324000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:6a9c1ecd8ec483bccd15fd7671e5bb9d0e19895419ceeba23656d9bbf7585dbe#1472816518403000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:868b1d0e2aadb964dc43beee5d8c2f27cbbfed78a9e2d2a2991a4095259e5e2f#1472811458890000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4#1472811576912000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:c0bfa94a72678b7c6e06821f15cccb5ebe7e0e10739bedbb2d80da872113cf22#1472816522233000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:c86498c1ddc3803270df2a21d626844ea9bdbb84fc0a43f300fed43b6c400437#1472811458796000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/images/sha256:ceb711c7e301352864df69931a5fa92b005f10713fa09c57ffe790f251234034#1472811570219000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/repositories/library/hello-node/manifest_sha256:0d798ac3bf8667e82aad2db34afce77a5d74a201f5e4d02b910f8bdfa383c231#1472811579483000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/repositories/library/hello-node/manifest_sha256:c42b83d239ef7b2733094cf4a782b83542484b444147939cc7d2cb7fa05b1074#1472816526483000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/repositories/library/hello-node/tag_v1#1472811580331000...
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/containers/repositories/library/hello-node/tag_v2#1472816527245000...
                        / [15 objects]                                                                  
                        Operation completed over 15 objects.                                             
                        Removing gs://artifacts.eedevs-kubernetes-1.appspot.com/...
                        <<<

